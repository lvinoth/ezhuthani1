package com.ezhuthani.ezhuthaniapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;


public class SplashActivity extends AppCompatActivity {

    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UtilsClass.makeStatusBarTransparent(getWindow(), this);
        setContentView(R.layout.activity_splash);
        showMainScreen();
    }

    private void showMainScreen() {

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Launch Main Activity here

                Intent i = new Intent(getBaseContext(), MainActivity.class);
                startActivity(i);
                finish();
            }
        }, 2000);
    }

}
