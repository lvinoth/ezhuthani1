package com.ezhuthani.ezhuthaniapp;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private String postUrl = "";
    private String baseUrl = "https://www.ezhuthani.com";
    private WebView webView;
    private FloatingActionButton shareBtn;
    private Button noInternetBtn;
    private RelativeLayout noInternetLayout;
    private Dialog mOverlayDialog;
    public static boolean isRunning = false;
    public Activity baseActivity;
    String source;
    boolean clearHistory = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UtilsClass.makeStatusBarTransparent(getWindow(), this);
        setContentView(R.layout.activity_main);
        isRunning = true;
        baseActivity = this;
        source = getIntent().getStringExtra("source");
        postUrl = baseUrl;
        if (source != null && source.equals("notification"))
        {
            String url = getIntent().getStringExtra("url_load");
            postUrl = url != null ? url : baseUrl;
        }
        webView = (WebView) findViewById(R.id.webView);
        noInternetBtn = (Button) findViewById(R.id.noInternetBtn);
        noInternetLayout = (RelativeLayout) findViewById(R.id.noInternetLayout);
        createLoader(this);
        registerReceiver(broadcastReceiver, new IntentFilter("new_article"));
        noInternetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOnline())
                {
                    noInternetLayout.setVisibility(View.GONE);
                    webView.loadUrl(postUrl);
                    Log.e(MainActivity.class.getSimpleName(), "Webpage is available!");
                }
                else {
                    noInternetLayout.setVisibility(View.VISIBLE);
                    webView.stopLoading();
                    Log.e(MainActivity.class.getSimpleName(), "oops! webpage is not available!");
                }
            }
        });

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setHorizontalScrollBarEnabled(false);

        shareBtn = (FloatingActionButton) findViewById(R.id.shareBtn);
        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String shareBody = "Visit https://www.ezhuthani.com to explore latest information and news";
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                sharingIntent.setType("text/plain");
                startActivity(Intent.createChooser(sharingIntent, "Invite your friends"));
            }
        });

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
//                progressBar.setProgress(progress);
            }
        });

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
//                progressBar.setVisibility(View.VISIBLE);
                mOverlayDialog.show();

            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
                super.doUpdateVisitedHistory(view, url, isReload);
                if (clearHistory)
                {
                    clearHistory = false;
                    webView.clearHistory();
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
//                progressBar.setVisibility(View.GONE);
                mOverlayDialog.dismiss();
               /* if (clearHistory)
                {
                    clearHistory = false;
                    webView.clearHistory();
                }*/
            }
        });

        if (isOnline())
        {
            noInternetLayout.setVisibility(View.GONE);
            webView.loadUrl(postUrl);
            Log.e(MainActivity.class.getSimpleName(), "Webpage is available!");
        }
        else {
            noInternetLayout.setVisibility(View.VISIBLE);
            webView.stopLoading();
            Log.e(MainActivity.class.getSimpleName(), "oops! webpage is not available!");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isRunning = false;
        unregisterReceiver(broadcastReceiver);
    }

    public void createLoader(Context context)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View progressLayout = inflater.inflate(R.layout.progress, null);

        mOverlayDialog = new Dialog(context);
        mOverlayDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mOverlayDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mOverlayDialog.setContentView(progressLayout);
        mOverlayDialog.setCancelable(false);
        mOverlayDialog.setCanceledOnTouchOutside(false);
    }

    private void createAlert()
    {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog_alert);

        TextView text = dialog.findViewById(R.id.messageView);
        text.setText("Do you want to close the app?");
        TextView yesBtn = dialog.findViewById(R.id.yesBtn);
        TextView noBtn = dialog.findViewById(R.id.noBtn);

        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
               baseActivity.finishAndRemoveTask();
            }
        });

        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }


    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent intent) {
            String action = intent.getAction();
            String newUrl = intent.getStringExtra("url");
            if (action.equals("new_article")) {
                webView.loadUrl(newUrl);
            }
        }
    };

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    @Override
    public void onBackPressed() {
        if (source != null && source.equals("notification"))
        {
            source = "";
            webView.loadUrl(baseUrl);
            clearHistory = true;
        }
        else {
            if (webView.canGoBack())
            {
                webView.goBack();
            }
            else {
                createAlert();
            }
        }
    }
}
