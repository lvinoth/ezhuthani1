package com.ezhuthani.ezhuthaniapp;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

public class NotificationHandler implements OneSignal.NotificationOpenedHandler {
    private Context mContext;

    NotificationHandler(Context context) {
        mContext = context;
    }

    @Override
    public void notificationOpened(OSNotificationOpenResult result) {

        Log.e(NotificationHandler.class.getSimpleName(), "Launch URL: " + result.notification.payload.launchURL);
        String launchURL = result.notification.payload.launchURL;


        if (MainActivity.isRunning) {
            Intent intent = new Intent("new_article");
            intent.putExtra("url", launchURL);
            mContext.sendBroadcast(intent);
        } else {
            Intent intent = new Intent(mContext, MainActivity.class);
            intent.putExtra("url_load", launchURL);
            intent.putExtra("source", "notification");
            PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            try {
                pendingIntent.send();
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            }
        }

    }
}
