package com.ezhuthani.ezhuthaniapp;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class NewArticleActivity extends AppCompatActivity {

    private WebView webView;
    private Button noInternetBtn;
    private RelativeLayout noInternetLayout;
    private Dialog mOverlayDialog;
    private String notificationURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UtilsClass.makeStatusBarTransparent(getWindow(), this);
        setContentView(R.layout.activity_new_article);

        notificationURL = getIntent().getStringExtra("url_load");
        Log.e(NewArticleActivity.class.getSimpleName(),"NotificationURL: " + notificationURL);

        webView = (WebView) findViewById(R.id.webViewNew);
        noInternetBtn = (Button) findViewById(R.id.noInternetBtn1);
        noInternetLayout = (RelativeLayout) findViewById(R.id.noInternetLayout);
        createLoader(this);
        noInternetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOnline())
                {
                    noInternetLayout.setVisibility(View.GONE);
                    webView.loadUrl(notificationURL);
                    Log.e(NewArticleActivity.class.getSimpleName(), "Webpage is available!");
                }
                else {
                    noInternetLayout.setVisibility(View.VISIBLE);
                    webView.stopLoading();
                    Log.e(NewArticleActivity.class.getSimpleName(), "oops! webpage is not available!");
                }
            }
        });

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setHorizontalScrollBarEnabled(false);

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
//                progressBar.setProgress(progress);
            }
        });

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
//                progressBar.setVisibility(View.VISIBLE);
                mOverlayDialog.show();

            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {

//                progressBar.setVisibility(View.GONE);
                mOverlayDialog.dismiss();
            }
        });

        if (isOnline())
        {
            noInternetLayout.setVisibility(View.GONE);
            webView.loadUrl(notificationURL);
            Log.e(NewArticleActivity.class.getSimpleName(), "Webpage is available!");
        }
        else {
            noInternetLayout.setVisibility(View.VISIBLE);
            webView.stopLoading();
            Log.e(NewArticleActivity.class.getSimpleName(), "oops! webpage is not available!");
        }
    }

    public void createLoader(Context context)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View progressLayout = inflater.inflate(R.layout.progress, null);

        mOverlayDialog = new Dialog(context);
        mOverlayDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mOverlayDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mOverlayDialog.setContentView(progressLayout);
        mOverlayDialog.setCancelable(false);
        mOverlayDialog.setCanceledOnTouchOutside(false);
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    @Override
    public void onBackPressed() {

        if (!MainActivity.isRunning){
//            Toast.makeText(this,"activity not running..", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
        /*else {
            Toast.makeText(this,"activity running..", Toast.LENGTH_SHORT).show();
        }*/

        this.finish();
    }

    @Override
    public void finish() {
        super.finish();
        Log.e(NewArticleActivity.class.getSimpleName(),"NewArticleActivity destroyed..");
    }
}
